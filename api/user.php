<?php

$users = array(
    1 => array(
        'id' => 1,
        'name' => 'Habsah bt Zan',
        'email' => 'a@b.c'
    ),
    2 => array(
        'id' => 2,
        'name' => 'Noor Irma bt Ruhani',
        'email' => 'a@b.c'
    ),
    3 => array(
        'id' => 3,
        'name' => 'Khairul b. Abd Rahman',
        'email' => 'a@b.c'
    )
);

exit(json_encode($users));
