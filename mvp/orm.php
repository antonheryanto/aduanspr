<?php
include './db.php';

class Orm extends Db {
    protected $table;

    public function __construct() {
        $this->table = get_class($this);
    }
    public function get($id) {
        $c = $this->query("select * from $this->table where id=:id", [':id' => $id ]);
        $c->setFetchMode(PDO::FETCH_CLASS, $this->table);
        return $c->fetch(PDO::FETCH_CLASS);
    }
    
    public function all() {
        return $this->query("select * from $this->table")->fetchAll(PDO::FETCH_CLASS, $this->table);
    }
    
    public function insert($data) {
        foreach ($data as $k => $v) {
            $fa[] = $k;
            $va[] = ":$k";
            $p[":$k"]=$v;
        }
        $f = implode(",", $fa);
        $v = implode(",", $va);
        $c = $this->query("insert into $this->table ($f) values ($v);select last_insert_id()", $p);
        $c->nextRowset();
        return $c->fetchColumn();
    }
    
    public function insertOrUpdate($id,$data) {
        foreach ($data as $k => $v) {
            $fa[] = $k;
            $va[] = ":$k";
            $p[":$k"]=$v;
            $ua[] = "$k=:$k";
        }
        $p[":id"]=$id;
        $f = implode(",", $fa);
        $v = implode(",", $va);
        $u = implode(",", $ua);
        $sql = "insert into $this->table ($f, id) values ($v, :id)"
            . "on duplicate key update $u ,id=last_insert_id(:id);select last_insert_id()";
        $c = $this->query($sql, $p);
        $c->nextRowset();
        return $c->fetchColumn();
    }
    
    public function update($id, $data) {
        foreach ($data as $k => $v) {
            $fa[] = "$k=:$k";
            $p[":$k"]=$v;
        }
        $p[":id"] = $id;
        $f = implode(",", $fa);
        return $this->execute("update $this->table set $f where id=:id", $p);
    }
    
    public function delete($id) {
        $r = $this->execute("delete from $this->table where id=:id",[":id" => $id]);
        return $r > 0;
    }
}
