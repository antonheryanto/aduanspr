<?php
include './ui.php';
$title = "Borang Aduan";
head($title); ?>
<div class="container">
    <h1>&nbsp;</h1>
    <h1><?=$title?></h1>
    <div class="panel panel-info">
        <div class="panel-heading">Borang Aduan</div>
        <form class="form-horizontal panel-body" method="POST" action="aduan/api.php">
            <div class="form-group">
                <label class="col-md-2 control-label">Perkara</label>
                <div class="col-md-10">
                    <input class="form-control"  name="title">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Aduan</label>
                <div class="col-md-10">
                    <textarea class="form-control" name="body"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Nama</label>
                <div class="col-md-10">
                    <input class="form-control"  name="name">
                </div>
            </div>
            <div class="form-group has-error">
                <label class="col-md-2 control-label">Email</label>
                <div class="col-md-10">
                    <input class="form-control" name="email">
                </div>
            </div>
            <div class="form-group has-success">
                <label class="col-md-2 control-label">IC</label>
                <div class="col-md-10">
                    <input class="form-control" name="ic">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <button class="btn btn-primary">Hantar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php 
foot();