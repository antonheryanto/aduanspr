<?php
include '../ui.php';
include './api.php';
$title = "Senarai Aduan";
head($title);
?>
<div class="container">
    <h1>&nbsp;</h1>
    <h1><?= $title ?></h1>
    <div class="panel panel-success">
        <div class="panel-heading">Senarai Aduan</div>
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Perkara</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>IC</th>
                    <th>Tarikh</th>
                    <th>Tindakan</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($items as $i) { ?>
                    <tr>
                        <td><?=$i['title']?></td>
                        <td><?=$i['name']?></td>
                        <td><?=$i['email']?></td>
                        <td><?=$i['ic']?></td>
                        <td><?=$i['created']?></td>
                        <td>
                            <a href="edit.php?id=<?=$i['id']?>">
                                <i class="glyphicon glyphicon-wrench"></i> 
                                Terima</a>
                        </td>
                    </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>  
<?php
foot();
    