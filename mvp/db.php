<?php

class Db {
    /**
     *  @var PDO 
     */
    protected static $cn = false;

    public function execute($sql, $params = NULL) {
        try {
            $cn = $this->connect();
            $q = $cn->prepare($sql);
            $q->execute($params);
            return $q->rowCount();
        } catch (PDOException $ex) {
            $this->debug($ex->getMessage());
        }
        return 0;
    }

    /**
     * 
     * @param string $sql
     * @param array $params
     * @return PDOStatement
     */
    public function query($sql, $params = NULL) {
        try {
            $cn = $this->connect();
            $q = $cn->prepare($sql);
            $q->execute($params);
            return $q;
        } catch (PDOException $ex) {
            $this->debug($ex->getMessage());
        }
        return NULL;
    }
    
    public function connect() {
        if (!self::$cn) {
            self::$cn = new PDO('mysql:host=127.0.0.1;dbname=t_spr', 'root','tante', [
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
            ]);
        }
        return self::$cn;
    }
    
    protected function debug($msg) {
        echo "<pre>Error!: $msg\n";
        $bt = debug_backtrace();
        foreach ($bt as $line) {
            $args = var_export($line['args'], true);
            echo "{$line['function']}($args) at {$line['file']}:{$line['line']}\n";
        }
        echo "</pre>";
        die();
    }
}

