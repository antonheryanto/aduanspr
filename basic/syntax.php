<?php

$string = "saya variable type string";
$int = 1;
$booelan = true;


$x = 0;
echo "isset ".(isset($x) ? "ada" : "tiada")."<br>";
echo "empty ".(!empty($x) ? "ada" : "tiada");

//id=0,id=1
if (isset($_GET['id'])) {
    echo ""; 
} else {
    echo "hide";
}
//itenary operator
echo isset($_GET['id']) ? "" : "hide";
$i = 10;
while($i > 0) {
    echo " $i, ";
    $i--;
}
for($j=0;$j<10;$j++) {
    echo " $j, ";
}
//          0  1  2
$names = ['a','b','c'];
foreach ($names as $v) {
    echo $v;
}
for($j=0;$j<sizeof($names);$j++) {
    $names[$j] .= " edited"; 
    $v = $names[$j];
    echo " $v, ";
}

foreach ($names as $i => $v) {
    echo $v;
}

$dict = ['id' => 1, 'name' => 'Anton'];
$p = [
    1 => ['id' => 1, 'name' => 'Anton'],
    2 => ['id' => 2, 'name' => 'Hasan']
];

foreach ($dict as $k => $v) {
    echo $k,$v;
}
