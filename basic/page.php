<?php 
$aduan = ['Anton', 'Heryanto','Hasan'];
$v = $aduan[$_GET['id']];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Page Based</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Aduan SPR</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="menu">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Aduan</a></li>
                    <li class="active"><a href="#">Selengara</a></li>
                </ul>
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Cari</button>
                </form>
                <form class="navbar-form navbar-left hide" role="login">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Kataluluan">
                    </div>
                    <button type="submit" class="btn btn-default">Login</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Anton Heryanto</a></li>
                    <li><a href="#">Keluar</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
        <div class="container">
            <h1>&nbsp;</h1>
            <a href="page.php" class="btn btn-primary">Kembali</a>
            <h1>Mockup</h1>
            <div class="panel panel-info <?=isset($_GET['id']) ? "" : "hide"?>">
                <div class="panel-heading">Borang Aduan</div>
                <form class="form-horizontal panel-body">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Aduan</label>
                        <div class="col-md-10">
                            <textarea class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Nama</label>
                        <div class="col-md-10">
                            <input class="form-control" value="<?=$v?>">
                        </div>
                    </div>
                    <div class="form-group has-error">
                        <label class="col-md-2 control-label">Email</label>
                        <div class="col-md-10">
                            <input class="form-control" value="<?=$v?>@gmail.com">
                        </div>
                    </div>
                    <div class="form-group has-success">
                        <label class="col-md-2 control-label">IC</label>
                        <div class="col-md-10">
                            <input class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button class="btn btn-primary">Hantar</button>
                        </div>
                    </div>
                </form>
            </div>
            <a href="?id=" class="btn btn-primary">Tambah</a>
            <div class="panel panel-success <?=isset($_GET['id']) ? "hide" : ""?>">
                <div class="panel-heading">Senarai Aduan</div>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>IC</th>
                        <th>Tarikh</th>
                        <th>Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($aduan as $id => $v) { ?>
                    <tr>
                        <td><?=$v?></td>
                        <td><?=$v?>@gmail.com</td>
                        <td>Tiada</td>
                        <td>Sekarang</td>
                        <td><a href="?id=<?=$id?>"><i class="glyphicon glyphicon-wrench"></i> Terima</a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            </div>  
            <h1>&nbsp;</h1>
            <h1>&nbsp;</h1>
        </div>
        
        <footer class="navbar navbar-inverse navbar-fixed-bottom" role="footer">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#footer">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">SPR &copy;2013</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="footer">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Hubungi Kami</a></li>
                    <li><a href="#">Terma dan Syarat</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </footer>

        <script src="/js/jquery-1.10.2.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
    </body>
</html>
