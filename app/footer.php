<footer class="navbar navbar-inverse navbar-fixed-bottom" role="footer">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#footer">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">SPR &copy;2013</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="footer">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Hubungi Kami</a></li>
                    <li><a href="#">Terma dan Syarat</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </footer>