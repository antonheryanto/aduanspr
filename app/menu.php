<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Aduan SPR</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="menu">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Aduan</a></li>
                    <li class="active"><a href="#">Selengara</a></li>
                </ul>
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Cari</button>
                </form>
                <form class="navbar-form navbar-left hide" role="login">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Kataluluan">
                    </div>
                    <button type="submit" class="btn btn-default">Login</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Anton Heryanto</a></li>
                    <li><a href="#">Keluar</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>