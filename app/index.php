<?php
$aduan = ['Anton', 'Heryanto', 'Hasan'];
$v = $aduan[$_GET['id']];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Page Based</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
    </head>
    <body>
    <?php include "./menu.php"; ?>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <a href="?id=" class="btn btn-primary">Tambah</a>
        <div class="panel panel-success">
            <div class="panel-heading">Senarai Aduan</div>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>IC</th>
                        <th>Tarikh</th>
                        <th>Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($aduan as $id => $v) { ?>
                        <tr>
                            <td><?= $v ?></td>
                            <td><?= $v ?>@gmail.com</td>
                            <td>Tiada</td>
                            <td>Sekarang</td>
                            <td><a href="edit.php?id=<?= $id ?>"><i class="glyphicon glyphicon-wrench"></i> Terima</a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>  
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
    </div>
    <?php include "./footer.php"; ?>  
    <script src="/js/jquery-1.10.2.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</body>
</html>
