<?php
$aduan = ['Anton', 'Heryanto', 'Hasan'];
$v = $aduan[$_GET['id']];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Page Based</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
    </head>
    <body>
    <?php include "./menu.php"; ?>
        <h1>&nbsp;</h1>
            <a href="page.php" class="btn btn-primary">Kembali</a>
            <h1>Mockup</h1>
            <div class="panel panel-info">
                <div class="panel-heading">Borang Aduan</div>
                <form class="form-horizontal panel-body">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Aduan</label>
                        <div class="col-md-10">
                            <textarea class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Nama</label>
                        <div class="col-md-10">
                            <input class="form-control" value="<?=$v?>">
                        </div>
                    </div>
                    <div class="form-group has-error">
                        <label class="col-md-2 control-label">Email</label>
                        <div class="col-md-10">
                            <input class="form-control" value="<?=$v?>@gmail.com">
                        </div>
                    </div>
                    <div class="form-group has-success">
                        <label class="col-md-2 control-label">IC</label>
                        <div class="col-md-10">
                            <input class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button class="btn btn-primary">Hantar</button>
                        </div>
                    </div>
                </form>
            </div>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
    </div>
    <?php include "./footer.php"; ?>  
    <script src="/js/jquery-1.10.2.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</body>
</html>
